<?php

namespace GrossbergerGeorg\TrackLoaded\Tests;

/*
 * (c) 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the Apache-2.0 license
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

use GrossbergerGeorg\TrackLoaded\Dumper;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;

/**
 * DumperTest
 *
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class DumperTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        require_once __DIR__ . '/StatusTrackerDummy.php';
    }

    public function testWriteData()
    {
        $knownClasses = [
            'Namespace\\One\\Main' => 0,
            'Namespace\\One\\Secondary' => 1,
        ];

        $knownNamespaces = [
            'Namespace\\One\\' => 0,
            'Namespace\\Two\\' => 1,
        ];

        $fs = vfsStream::setup('typo3', null, ['list_new.php' => '<?php return []; ',]);


        $file = $fs->getChild('list_new.php')->url();
        $target = str_replace('_new.', '_{key}.', $file);

        $subject = new Dumper($knownClasses, $knownNamespaces);
        $subject->setDeclaredIdentifiers([
            [
                'Namespace\\One\\Main',
                'Namespace\\One\\Secondary',
                'GrossbergerGeorg\\TrackLoaded\\StatusTracker'
            ],
            [
                'Namespace\\Two\\Three\\Four\\Main',
            ]
        ]);
        $subject->writeData($target);

        $code = file_get_contents($file);

        static::assertStringContainsString('\'Namespace\\One\\Main\'', $code);
        static::assertStringContainsString('\'Namespace\\One\\Secondary\'', $code);
        static::assertStringContainsString('\'Namespace\\Two\\Three\\Four\\Main\'', $code);
        static::assertStringNotContainsString('\'GrossbergerGeorg\\TrackLoaded\\StatusTracker\'', $code);
    }
}
