<?php

namespace GrossbergerGeorg\PreloadTools;

/*
 * (c) 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the Apache-2.0 license
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class StatusTracker
{
    public function getRuntimeModeName(): string
    {
        return 'new';
    }
}
