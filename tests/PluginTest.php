<?php

namespace GrossbergerGeorg\TrackLoaded\Tests;

/*
 * (c) 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the Apache-2.0 license
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

use Composer\Composer;
use Composer\Config;
use Composer\IO\NullIO;
use Composer\Package\RootPackage;
use Composer\Script\Event;
use GrossbergerGeorg\TrackLoaded\Plugin;
use org\bovigo\vfs\vfsStream;
use PHPUnit\Framework\TestCase;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class PluginTest extends TestCase
{

    public function testAppendDumper()
    {
        $suffix = '__unique_prefix__';

        $fs = vfsStream::setup('typo3', null, [
            'var' => [
                'list.php' => '<?php exit(1);'
            ],
            'public' => [
                'index.php' => "<?php\nrequire 'bootstrap.php'\n",
            ],
            'vendor' => [
                'autoload.php' => '<?php require "composer/autoload.php"; ComposerAutoloaderInit' . $suffix .  '::$classMap = [];'
            ],
        ]);

        $extra = [
            'grossberger-georg/typo3-track-loaded' => [
                'target-file' => 'var/list.php',
            ],
        ];

        $config = new Config();
        $config->merge([
            'config' =>[
                'vendor-dir' => $fs->getChild('vendor')->url(),
                'autoloader-suffix' => '',
            ],
        ]);

        $package = $this->createMock(RootPackage::class);
        $package->expects(static::any())->method('getExtra')->willReturn($extra);

        $composer = $this->createMock(Composer::class);
        $composer->expects(static::any())->method('getPackage')->willReturn($package);
        $composer->expects(static::any())->method('getConfig')->willReturn($config);

        $event = $this->createMock(Event::class);
        $event->expects(static::any())->method('getIO')->willReturn(new NullIO());
        $event->expects(static::any())->method('getComposer')->willReturn($composer);

        $subject = new Plugin();
        $subject->appendDumper($event);

        $code = file_get_contents($fs->getChild('public/index.php')->url());

        static::assertStringContainsString('ComposerStaticInit' . $suffix, $code);
    }
}
