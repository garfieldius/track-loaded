<?php
declare(strict_types=1);
namespace GrossbergerGeorg\TrackLoaded;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Composer\Script\Event;
use Composer\Script\ScriptEvents;
use Composer\Util\Filesystem;

/**
 * Plugin that appends the dumper to the index.php file
 *
 * @author Georg Großberger <g.grossberger@supseven.at>
 */
class Plugin implements PluginInterface, EventSubscriberInterface
{
    /**
     * @param Composer $composer
     * @param IOInterface $io
     */
    public function activate(Composer $composer, IOInterface $io)
    {
        // noop
    }

    /**
     * Listen to the post-autoload event
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            ScriptEvents::POST_AUTOLOAD_DUMP => ['appendDumper'],
        ];
    }

    /**
     * Append the dumper to the index.php of the frontend entry point
     *
     * By default, this is public/index.php
     *
     * @param \Composer\Script\Event $event
     */
    public function appendDumper(Event $event)
    {
        $event->getIO()->write('<info>Adding classes dumper to index.php</info>');
        $extra = $event->getComposer()->getPackage()->getExtra();
        $fs = new Filesystem();
        $config = $event->getComposer()->getConfig();
        $vendorPath = $fs->normalizePath($config->get('vendor-dir'));

        $suffix = $config->get('autoloader-suffix');
        $target = $extra['grossberger-georg/typo3-track-loaded']['target-file'] ?? 'var/loaded_{key}.php';

        if (!$suffix && is_readable($vendorPath . '/autoload.php')) {
            $content = file_get_contents($vendorPath . '/autoload.php');

            if (preg_match('{ComposerAutoloaderInit([^:\\s]+)::}', $content, $match)) {
                $suffix = $match[1];
            }
        }

        if (!$suffix) {
            $suffix = md5(uniqid('', true));
        }

        $startToken = '// grossberger-georg/typo3-track-loaded start';
        $endToken = '// grossberger-georg/typo3-track-loaded end';

        $code = <<<EOF

${startToken}

(new \\GrossbergerGeorg\\TrackLoaded\\Dumper(
    array_flip(array_keys(\\Composer\\Autoload\\ComposerStaticInit${suffix}::\$classMap)),
    array_flip(array_keys(\\Composer\\Autoload\\ComposerStaticInit${suffix}::\$prefixDirsPsr4))
))->setDeclaredIdentifiers([
    get_declared_classes(),
    get_declared_interfaces(),
    get_declared_traits(),
])->writeData(__DIR__ . '/../${target}');

${endToken}

EOF;
        $code = trim($code) . "\n";

        $webDir = $extra['typo3/cms']['web-dir'] ?? 'public';
        $file = dirname($vendorPath) . '/' . trim($webDir, '/') . '/index.php';

        if (is_file($file)) {
            $content = file_get_contents($file);

            if (strpos($content, $startToken) !== false) {
                $lines = explode("\n", $content);
                $content = '';
                $skip = false;

                while (count($lines)) {
                    $line = array_shift($lines);

                    if (trim($line) === $startToken) {
                        $skip = true;
                    } elseif ($skip) {
                        $skip = trim($line) !== $endToken;
                    } else {
                        $content .= $line . "\n";
                    }
                }
            }

            $content .= $code;
            file_put_contents($file, $content);
        }
    }
}
