<?php
declare(strict_types=1);
namespace GrossbergerGeorg\TrackLoaded;

/*
 * Copyright 2020 by Georg Großberger <contact@grossberger-ge.org>
 *
 * This is free software; it is provided under the terms of Apache License 2.0
 * See the file LICENSE or <https://www.apache.org/licenses/LICENSE-2.0> for details
 */

use GrossbergerGeorg\PreloadTools\StatusTracker;
use TYPO3\CMS\Core\Utility\ArrayUtility;

/**
 * Write the fully qualified class names of all loaded classes into a php array
 *
 * @author Georg Großberger <g.grossberger@supseven.at>
 */
class Dumper
{
    /**
     * @var array
     */
    private $classMap;

    /**
     * @var array
     */
    private $psr4Prefixes;

    /**
     * @var array
     */
    private $declaredIdentifiers = [];

    /**
     * Dumper constructor.
     * @param array $classMap
     * @param array $psr4Prefixes
     */
    public function __construct(array $classMap, array $psr4Prefixes)
    {
        $this->classMap = $classMap;
        $this->psr4Prefixes = $psr4Prefixes;
    }

    /**
     * @param array $declaredIdentifiers
     * @return Dumper
     */
    public function setDeclaredIdentifiers(array $declaredIdentifiers): Dumper
    {
        $this->declaredIdentifiers = $declaredIdentifiers;

        return $this;
    }

    public function writeData($target): void
    {
        if (strpos($target, '{key}') !== false) {
            $key = 'new';

            if (class_exists(StatusTracker::class)) {
                $key = (new StatusTracker())->getRuntimeModeName();
            }

            $target = str_replace('{key}', $key, $target);
        }

        $data = [];

        if (is_file($target)) {
            $data = (array) require $target;
        }

        $data = $this->collectClasses($data);

        $config = ArrayUtility::arrayExport($data);
        $code = <<<EOF
<?php
return ${config};
EOF;

        file_put_contents($target, ltrim($code));
    }

    private function collectClasses(array $classes): array
    {
        foreach ($this->declaredIdentifiers as $set) {
            foreach ($set as $identifier) {
                if (
                    strpos($identifier, 'GrossbergerGeorg\\TrackLoaded\\') !== false
                    || strpos($identifier, 'GrossbergerGeorg\\PreloadTools\\') !== false
                    || strpos($identifier, 'Composer\\Autoload\\') !== false
                    || strpos($identifier, 'TYPO3\\ClassAliasLoader\\') !== false
                ) {
                    continue;
                }

                $found = isset($this->classMap[$identifier]);

                if (!$found) {
                    $parts = explode('\\', $identifier);
                    array_pop($parts);

                    while (count($parts) && !$found) {
                        $namespace = implode('\\', $parts) . '\\';

                        if (isset($this->psr4Prefixes[$namespace])) {
                            $found = true;
                        } else {
                            array_pop($parts);
                        }
                    }
                }

                if ($found) {
                    if (empty($classes[$identifier])) {
                        $classes[$identifier] = 0;
                    }

                    $classes[$identifier]++;
                }
            }
        }

        return $classes;
    }
}
