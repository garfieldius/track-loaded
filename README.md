# Track loaded classes in TYPO3 frontend

This is a composer plugin which adds a "class-dumper" into the index.php
file of the frontend. It will generate a list of all loaded classes,
interfaces and traits to be used by the preload_tools extension.

See package [grossberger-georg/typo3-preload-tools](https://githubl.com/garfieldius/typo3-preload-tools) for details.

## License

[Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)
